function init() {
    var user_ID = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var userstate = document.getElementById('dynamic-btn');
        // Check user login
        if (user) {
            user_ID = user.email.split("@",1);
            userstate.innerHTML = user_ID;
            userstate.href = "index.html";

            document.getElementById("dynamic-btn").addEventListener("click", function () {
                firebase.auth().signOut().then(function () {
                    //create_alert("success", "");
                    alert("sueccess");
                }).catch(function (error) {
                    //create_alert("error", error.code, error.message);
                    alert("failed");
                });
            });
        } else {
            // It won't show any post if not login
            userstate.innerHTML = "Login";
            userstate.href = "signin.html";
            document.getElementById('chat-window').innerHTML = "";
        }
    });

    var postsRef = firebase.database().ref('chat_list');
    var show  = document.getElementById('chat-window');
    var memberRef = firebase.database().ref('member_list');
    var display = document.getElementById('members');

    name_btn = document.getElementById('username-btn');
    name_txt = document.getElementById('username');
    
    name_btn.addEventListener('click', function(){
        if(name_txt.value != ""){
            firebase.database().ref("member_list").push({
                ID: user_ID[0],
                name: name_txt.value
            }).catch(e => console.log(e.message));
        }
    });

    post_btn = document.getElementById('summit');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var date = new Date();
            var h = date.getHours();
            var m = date.getMinutes();
            if(h<10)    h = '0' + h;
            if(m<10)    m = '0' + m;
            var now = h + ':' + m;
            

            firebase.database().ref("chat_list").push({
                ID: user_ID[0],
                name: name_txt.value,
                data: post_txt.value,
                time: now
            }).catch(e => console.log(e.message));

            post_txt.value = "";

            var scrollHeight = $('#chat-window').prop("scrollHeight");
            $('#chat-window').scrollTop(scrollHeight,200);
        }
    });

    /*memberRef.once('value', function(snapshot){
        display.innerHTML = "";
        for(var i in snapshot.val()){
            $(display).append("<p><strong>"+snapshot.val()[i].name+"</strong></p>");
        }
    });
    memberRef.limitToLast(1).on('value', function(snapshot){
        for(var i in snapshot.val()){
            $(display).append("<p><strong>"+snapshot.val()[i].name+"</strong></p>");
        }
    });*/
    memberRef.on('value', function(snapshot) {
        display.innerHTML = "";
        var memberbase = [];
        var same_user = false;
        for(var i in snapshot.val()){
            same_user = false;
            for(var j=0; j<memberbase.length ; j=j+2){
                if( snapshot.val()[i].ID == memberbase[j]){
                    memberbase[j+1] = snapshot.val()[i].name;
                    same_user = true;
                    break;
                }
            }
            if(!same_user){
                memberbase.push(snapshot.val()[i].ID);
                memberbase.push(snapshot.val()[i].name);
            }

        }
        for(var j=0; j<memberbase.length ; j=j+2){
            $(display).append("<p><strong>"+memberbase[j+1]+"</strong></p>");
        }
    });

    postsRef.once('value', function(snapshot){
        show.innerHTML = "";
        for(var i in snapshot.val()){
            var str = snapshot.val()[i].data;
            str = str.replace(/\n/g, "<br>");
            if(snapshot.val()[i].ID == user_ID)
                $(show).append("<div style='overflow:hidden; float:right; text-align:right; clear:both;'><p class='mine'>"+snapshot.val()[i].name+"</p><span class='mine-timer'>"+snapshot.val()[i].time+"</span><span class='mine-senten'>"+str+"</span></div>");
            else
                $(show).append("<div style='overflow:hidden; clear:both;'><p class='name'>"+snapshot.val()[i].name+"</p><span class='sentence'>"+str+"</span><span class='timer'>"+snapshot.val()[i].time+"</span></div>");
        }
    });
    postsRef.limitToLast(1).on('value', function(snapshot){
        for(var i in snapshot.val()){
            var str = snapshot.val()[i].data;
            str = str.replace(/\n/g, "<br>");
            if(snapshot.val()[i].ID == user_ID)
                $(show).append("<div style='overflow:hidden; float:right; text-align:right; clear:both;'><p class='mine'>"+snapshot.val()[i].name+"</p><span class='mine-timer'>"+snapshot.val()[i].time+"</span><span class='mine-senten'>"+str+"</span></div>");
            else
                $(show).append("<div style='overflow:hidden; clear:both;'><p class='name'>"+snapshot.val()[i].name+"</p><span class='sentence'>"+str+"</span><span class='timer'>"+snapshot.val()[i].time+"</span></div>");
        }
        
    })
}

window.onload = function () {
    init();
};
