# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Chatroom
    我這次是模仿巴哈聊天室的風格做的群組聊天室。巴哈聊天室的風格是就算沒登入沒加入聊天室也能看到留言內容，不會顯示在線成員，全看聊天內容，有興趣就加入。我盡可能的模仿這種風格做了Midterm-Project。

* Key functions (add/delete)
    1. Chat
    2. Load history message
    3. Chat with new user

* Other functions (add/delete)
    1. Member list
    2. Name change
    3. Set scroll bar at top
    4. Distinguish my message and others message
    5. Message time
    6. Auto newline
    7. Same bottom of login and logout

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|

    E-mail signin signup 我有實作完成，是用Lab06的方法做的

|GitLab Page|5%|N|

    我的project用Node.js不知道為甚麼，都照著投影片做還是沒辦法deploy成功，但是用xampp可以在local端成功運行。

|Database|15%|N|

    Write的部分我有存member list跟message list，網路上有使用<form>+<input>&<textarea>的方法，但似乎在讀大寫英文跟中文的時候會出狀況，所以我是用clickeventlistener做的。
    Read的部分member list是使用database.ref().on來做，而message list則是使用.ref().once加上.ref().limittolast來做。會用兩種不同的做法是因為我本來打算在更改暱稱的時候將之前所有的舊暱稱都改成新的，但是因為message的部分數量有點多，怕會因為on的重複刷新導致延遲，所以用once + limittolast印一次就好，而member因為數量沒有那麼多所以可以用on來重複刷新名稱。

|RWD|15%|N|

    由於時間緊迫加上怕把自己的project搞壞所以RWD我沒有做。

|Topic Key Function|15%|N|

    chat及load history message的部分是使用once將放在message list裡的message都讀取印出，之後再增加的message就用limittolast加上去，避免on刷新全部留言造成的延遲。
    至於chat with new user的部分由於我是將所有成員放在同樣的聊天室，所以login之後就會自動加入....

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|

    這部分我有實作google的登入，一樣是照Lab06做的。

|Chrome Notification|5%|N|

    這部分因為時間緊湊我沒有做。

|Use CSS Animation|2.5%|N|

    動畫我有include firework的動畫插件，因為是找來的插件，我也沒辦法詳細說明煙火的運動軌跡函數是怎麼取的...

|Security Report|5%|N|

    因為巴哈聊天室的風格，所以我做成未登入可以讀但不能留言，但是因為我讀database的input需要讀登入的ID(就是email,為了做暱稱改變)，所以就算用default的security rules也能阻止未登入者留言及使用暱稱，我就用自己的方式做出security了.....

|Other functions|1~10%|N|

    Names&Member list:我有提供留言者自行改變暱稱的功能，聊天室的member list會在暱稱改變時及時更新。因為我覺得update的語法偏麻煩，所以我就用for loop自己繞開了，成員暱稱有改變，但實際上database member list是沒有變的。雖然成員的暱稱會即時改變，但是留言的舊暱稱不會改變，這在上面有提過，這是因為我的message list為了避免延遲是用once+limittolast做的，所以就算有更改database的內容也不會將對話框全部刷新。

    Scroll bar：我的聊天室的對話框會在留言長度超過畫框長度時產生直向卷軸，而且卷軸會一直停在最新留言的位置，這是使用scrolltop() function實作的。

    Mine Message and others:我在對話框內有區分出自己的留言跟其他人的留言，就像line或message那樣左右顏色分開顯示，因為是讀login email ID來判斷的，所以就算暱稱相同也能分出來。

    Message time:每個對話框的留言都會顯示留言時間，這是用Date()做的。

    same bottom of login and logout：我的login及logout是使用相同的按鈕，未登入時是login，登入後顯示email ID並且按下會logout，這是在changestate的listener中對bottom的value及herf進行改變


## Website Detail Description

## Security Report (Optional)

